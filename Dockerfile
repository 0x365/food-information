FROM node:current-alpine3.12

LABEL description="This container have food notes."
COPY . .
WORKDIR /docs
RUN npm install -g docsify-cli@latest
EXPOSE 3000/tcp
ENTRYPOINT docsify serve .